﻿window.publisherApp.controller('IndexController', ['$scope', '$http', function ($scope, $http) {
    $scope.dayOfMonth = 15

    // Set the day of the month to run the jobs
    $scope.submit = function () {
        if ($scope.schedulerForm.$invalid)
            return console.log('Invalid form');

        $http.post('/api/scheduler/dates?dayOfMonth=' + $scope.dayOfMonth, {}, { headers: { 'Content-Type': 'application/json' } })
            .success(function (data, status) {
                console.log('Submitted date', $scope.dayOfMonth, data, status)
            })
            .error(function (msg, status) {
                alert('Error: ' + status + '. ' + msg.msg)
            });
    };

    // Run now
    $scope.now = function () {
        console.log('Got here immediately');
        $http.post('/api/scheduler/now', {}, { headers: { 'Content-Type': 'application/json' } })
            .success(function (data, status) {
                console.log('Submitted immediately', data, status)
            })
            .error(function (msg, status) {
                alert('Error: ' + status + '. ' + JSON.stringify(msg))
            });
    };
}]);