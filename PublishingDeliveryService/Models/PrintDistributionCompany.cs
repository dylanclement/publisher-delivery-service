﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PublishingDeliveryService.Models
{
    public class PrintDistributionCompany
    {
        public int PrintDistributionCompanyId { get; set; }
        [Required]
        public string Name { get; set; }
    }
}