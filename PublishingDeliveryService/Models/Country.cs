﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PublishingDeliveryService.Models
{
    public class Country
    {
        public int CountryId { get; set; }
        [Required]
        public string Name{ get; set; }
    }
}