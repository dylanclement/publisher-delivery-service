﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PublishingDeliveryService.Models
{
    public class Publication
    {
        public int PublicationId { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string File { get; set; }

        public virtual List<Subscription> Subscriptions { get; set; }
        public virtual List<PrintMapping> PrintMappings { get; set; }
    }
}