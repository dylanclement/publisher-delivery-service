﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PublishingDeliveryService.Models
{
    public class Address
    {
        public int AddressId { get; set; }
        [Required]
        public string Line1 { get; set; }
        public string Line2 { get; set; }
        public string Line3 { get; set; }
        public string Suburb { get; set; }
        [Required]
        public string City { get; set; }
        public string State { get; set; }
        public int CountryId { get; set; }
        public virtual Country Country { get; set; }
    }
}