﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PublishingDeliveryService.Models
{
    public class Subscription
    {
        public int SubscriptionId { get; set; }
        public int CustomerId { get; set; }
        public int PublicationId { get; set; }
        public int DeliveryAddressId { get; set; }
        
        public virtual Customer Customer { get; set; }
        public virtual Publication Publication { get; set; }
        public virtual Address DeliveryAddress { get; set; }
    }
}