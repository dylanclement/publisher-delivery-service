﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace PublishingDeliveryService.Models
{
    public class PrintMapping
    {
        public int PrintMappingId { get; set; }
        [Index("UX_UniqueMapping", 1, IsUnique = true)]
        public int PublicationId { get; set; }
        [Index("UX_UniqueMapping", 2, IsUnique = true)]
        public int PrintDistributionCompanyId { get; set; }
        [Index("UX_UniqueMapping", 3, IsUnique = true)]
        public int CountryId { get; set; }

        public virtual Publication Publication { get; set;}
        public virtual PrintDistributionCompany PrintDistributionCompany { get; set; }
        public virtual Country Country { get; set; }
    }
}