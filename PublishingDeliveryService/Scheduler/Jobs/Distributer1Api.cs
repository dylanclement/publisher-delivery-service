﻿using PublishingDeliveryService.Services;
using Quartz;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace PublishingDeliveryService.Scheduler.Jobs
{
    public class Distributer1Job : IJob
    {
        //private IPublisherContext publisherContext;

        //public Distributer1Job(IPublisherContext _publisherContext)
        //{
        //    publisherContext = _publisherContext;
        //}

        public void Execute(IJobExecutionContext context)
        {
            var resolver = GlobalConfiguration.Configuration.DependencyResolver;
            var publisherContext = (IPublisherContext)resolver.GetService(typeof(IPublisherContext));

            Console.WriteLine("DistributionJob 1 is executing.");
            var httpWebRequest = (HttpWebRequest)WebRequest.Create("http://localhost:55941/api/scheduler/test");
            httpWebRequest.ContentType = "text/json";
            httpWebRequest.Method = "GET";

            // Get the distributon company name
            var distributionCentre = context.JobDetail.JobDataMap.GetString("distributionCompany");

            // Get the distributer for this job
            var distributer = from dist in publisherContext.PrintDistributionCompanies
                              where dist.Name == distributionCentre
                              select dist;

            // Find all Subscriptions belonging to this distributer
            var subscriptions = from pm in publisherContext.PrintMappings
                                where pm.PrintDistributionCompany == distributer
                                select pm.Publication.Subscriptions;


            //using (StreamWriter streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            //{
            //     Add subscriptions for this distributer to the stream based on what their API format is
            //    string json = "{\"name\": \"Test\"}";

            //    streamWriter.Write(json);
            //}

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var responseText = streamReader.ReadToEnd();
                //Now you have your response.
                //or false depending on information in the response
            }
        }

    }
}
