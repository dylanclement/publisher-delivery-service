﻿using Autofac;
using Autofac.Integration.WebApi;
using PublishingDeliveryService.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web.Http;

namespace PublishingDeliveryService
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            SetupIOC();
        }

        private static void SetupIOC()
        { 
            //Setup autofac IOC container
            var builder = new ContainerBuilder();
            // Register the Web API controllers.
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());

            builder.RegisterInstance(new SchedulerService())
                .As<ISchedulerService>()
                .OnActivated(e => e.Instance.Start());
            builder.RegisterInstance(new PublisherContext())
                .As<IPublisherContext>();

            var container = builder.Build();

            // Create the depenedency resolver.
            var resolver = new AutofacWebApiDependencyResolver(container);

            // Configure Web API with the dependency resolver.
            GlobalConfiguration.Configuration.DependencyResolver = resolver;
        }
    }
}
