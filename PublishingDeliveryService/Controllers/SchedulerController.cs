﻿using PublishingDeliveryService.Scheduler.Jobs;
using PublishingDeliveryService.Services;
using Quartz;
using Quartz.Impl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Web.Http;

namespace PublishingDeliveryService.Controllers
{
    public class SchedulerController : ApiController
    {
        ISchedulerService _scheduler;

        public SchedulerController(ISchedulerService scheduler)
        {
            _scheduler = scheduler;
        }

        /// <summary>
        /// Helper to generate an HTTP response
        /// </summary>
        /// <param name="responce"></param>
        /// <param name="httpStatus"></param>
        /// <returns></returns>
        private HttpResponseMessage CreateResponse(HttpRequestMessage request, string msg, HttpStatusCode httpStatus)
        {
            var response = request.CreateResponse(httpStatus);
            response.Content = new StringContent(msg, Encoding.UTF8, "application/json");
            return response;
        }


        public HttpResponseMessage PostDates(int dayOfMonth)
        {
            Console.WriteLine("Got here " + dayOfMonth);
            if (dayOfMonth < 1 || dayOfMonth > 28)
            {
                return CreateResponse(this.Request, "{\"success\": false, \"dayOfMonth\": " + dayOfMonth + ", \"msg\": \"Please pick a day of the month between 1 and 28\"}", HttpStatusCode.NotAcceptable);
            }
            try
            {
                _scheduler.AddCronJob<Distributer1Job>("distribution1", "0 0 0 " + dayOfMonth + " * ?");
            }
            catch (SchedulerException se)
            {
                Console.WriteLine(se);
                return CreateResponse(this.Request, "{\"success\": false}", HttpStatusCode.NotAcceptable);
            }
            return CreateResponse(this.Request, "{\"success\": true, \"dayOfMonth\": " + dayOfMonth + "}", HttpStatusCode.OK);
        }

        public HttpResponseMessage PostNow()
        {
            System.Console.WriteLine("Running Now");
            try
            {
                _scheduler.AddJobNow<Distributer1Job>("distribution1");
            }
            catch (SchedulerException se)
            {
                Console.WriteLine(se);
                return CreateResponse(this.Request, "{\"success\": false}", HttpStatusCode.NotAcceptable);
            }
            return CreateResponse(this.Request, "{\"success\": true}", HttpStatusCode.OK);
        }

        public HttpResponseMessage GetTest()
        {
            Console.WriteLine("Test Message");
            return CreateResponse(this.Request, "{\"success\": \"true\"}", HttpStatusCode.OK);
        }
    }
}
