﻿using Quartz;
using Quartz.Impl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PublishingDeliveryService.Services
{
    public interface ISchedulerService
    {
        void AddCronJob<T>(string name, string cron) where T : Quartz.IJob;
        void AddJobNow<T>(string name) where T : Quartz.IJob;

        void Start();
        void Stop();
    }

    public class SchedulerService: ISchedulerService
    {
        static IScheduler scheduler = new StdSchedulerFactory().GetScheduler();

        public void AddCronJob<T>(string name, string cron) where T : Quartz.IJob
        {
            IJobDetail job = JobBuilder.Create<T>()
                    .WithIdentity(name, "sendDistributionMessages")
                    .UsingJobData("distributionCompany", name)
                    .Build();

            // Trigger the job to run now, and then every 40 seconds
            ITrigger trigger = TriggerBuilder.Create()
              .WithIdentity(name, "sendDistributionMessages")
              .StartNow()
              .WithCronSchedule(cron)
              .ForJob(name, "sendDistributionMessages")
              .Build();

            scheduler.ScheduleJob(job, trigger);
        }

        public void AddJobNow<T>(string name) where T : Quartz.IJob
        {
            IJobDetail job = JobBuilder.Create<T>()
                    .WithIdentity(name, "sendDistributionMessages")
                    .UsingJobData("distributionCompany", name)
                    .Build();

            // Trigger the job to run now, and then every 40 seconds
            ITrigger trigger = TriggerBuilder.Create()
              .WithIdentity(name, "sendDistributionMessages")
              .StartNow()
              .WithSimpleSchedule(x => x
                  .WithIntervalInSeconds(10)
                  .RepeatForever())
              .ForJob(name, "sendDistributionMessages")
              .Build();
            
            scheduler.ScheduleJob(job, trigger);
        }

        public void Start()
        {
            scheduler.Start();
        }

        public void Stop()
        {
            scheduler.Start();
        }

    }
}