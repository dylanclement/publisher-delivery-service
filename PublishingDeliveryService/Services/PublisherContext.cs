﻿using PublishingDeliveryService.Models;
using System.Data.Entity;

namespace PublishingDeliveryService.Services
{
    public interface IPublisherContext
    {
        DbSet<Address> Adresses { get; set; }
        DbSet<Country> Countries { get; set; }
        DbSet<Customer> Customers { get; set; }
        DbSet<PrintDistributionCompany> PrintDistributionCompanies { get; set; }
        DbSet<PrintMapping> PrintMappings { get; set; }
        DbSet<Publication> Publications { get; set; }
        DbSet<Subscription> Subscriptions { get; set; }
    }

    public class PublisherContext : DbContext, IPublisherContext
    {
        public DbSet<Address> Adresses { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<PrintDistributionCompany> PrintDistributionCompanies { get; set; }
        public DbSet<PrintMapping> PrintMappings { get; set; }
        public DbSet<Publication> Publications { get; set; }
        public DbSet<Subscription> Subscriptions { get; set; }
    }
}